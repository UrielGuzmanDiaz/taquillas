package Taquillas;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

/**
 *
 * @author Uriel
 */
public class Control {
    public VistaTaquillas vt;
    public BoletosAgotados Agotados;
    public Control() //Se crea metodo constructor para poder llamar las clases que se utilizaran
    {
     this.vt=new VistaTaquillas();
     this.Agotados= new BoletosAgotados();
     
     
      InicianThreads();
    }
    /*public void iniciarVista(){
    vt.setTitle("Libreria");
    vt.pack();
    vt.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    vt.setLocationRelativeTo(null);
    vt.setVisible(true);  
    }*/

   
    public void InicianThreads(){
        
        //se invoca la clase  CuentaTaquillas que contiene la accion de los 5 hilos 
        CuentaTaquillas t1 = new CuentaTaquillas(vt,Agotados);
        //son el setName() entramos al switch de la clase cuenta boletos
        t1.setName("Taquilla1");
        
        CuentaTaquillas t2 = new CuentaTaquillas(vt,Agotados);
        
        t2.setName("Taquilla2");   
        
        CuentaTaquillas t3 = new CuentaTaquillas(vt,Agotados);
      
        t3.setName("Taquilla3");
        
        CuentaTaquillas t4 = new CuentaTaquillas(vt,Agotados);
        
        t4.setName("Taquilla4");
        
        CuentaTaquillas t5 = new CuentaTaquillas(vt,Agotados);
       
        t5.setName("Taquilla5");
        
        
         //Hacemos que los hilos corran   
         t1.start();
        t2.start();
       t3.start();
      t4.start();
    t5.start();
    
    
  
   }
    
}
     
