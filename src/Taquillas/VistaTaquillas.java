/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Taquillas;
//
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


/**
 *
 * @author Uriel
 */
public class VistaTaquillas extends JFrame {
    
    
    //Declaracion del panel
    public JPanel p; 
    
    //Declarion de labels que representaran las taquillas        
    public JLabel jl1;
    public JLabel jl2;
    public JLabel jl3;
    public JLabel jl4;
    public JLabel jl5;
    public JLabel jl6;
    public JLabel jl7;
    public JLabel jl8;
    public JLabel jl9;
    public JLabel jl10;
    //Declaracion de los JTextField
    public JTextField tq1,tq2,tq3,tq4,tq5,tq6,tq7,tq8,tq9,tq10,tq11,tq12,tq13,tq14,tq15,bts;
    
    public JButton btn;
    
    
    public VistaTaquillas(){
     jl1=new JLabel("TAQUILLA 1");
     jl2=new JLabel("TAQUILLA 2");
     jl3=new JLabel("TAQUILLA 3");
     jl4=new JLabel("TAQUILLA 4");
     jl5=new JLabel("TAQUILLA 5");
     jl6=new JLabel("BOLETOS DISPONIBLES PARA CADA TAQUILLA");  
     jl7=new JLabel("BOLETOS VENDIDOS EN:");
     jl8=new JLabel("COSTOS DE BOLETOS:  4DX  $160   GENERAL $60    3D $80   ");
     jl9=new JLabel("BOLETOS RESTANTES");
     jl10=new JLabel("ESTADO");
     //Haciendo los campos de texto no editables
     tq1=new JTextField(15);
     tq1.setEditable(false);
     tq2=new JTextField(15);
     tq2.setEditable(false);
     tq3=new JTextField(15);
     tq3.setEditable(false);
     tq4=new JTextField(15);
     tq4.setEditable(false);
     tq5=new JTextField(15);
     tq5.setEditable(false);
     tq6=new JTextField(10);
     tq6.setEditable(false);
     tq7=new JTextField(10);
     tq7.setEditable(false);
     tq8=new JTextField(10);
     tq8.setEditable(false);
     tq9=new JTextField(10);
     tq9.setEditable(false);
     tq10=new JTextField(10);
     tq10.setEditable(false);
     tq11=new JTextField(15);
     tq11.setEditable(false);
     tq12=new JTextField(15);
     tq12.setEditable(false);
     tq13=new JTextField(15);
     tq13.setEditable(false);
     tq14=new JTextField(15);
     tq14.setEditable(false);
     tq15=new JTextField(15);
     tq15.setEditable(false);
     bts=new JTextField(15);
     bts=new JTextField("100");
     bts.setEditable(false);
   //bts=new JTextField();
     
     btn=new JButton("Iniciar");
     //bts=new JTextField("1000");
     
     
     
     p=new JPanel();
     
     //vista();
    
    //public void vista(){
        this.setSize(500,400);
        this.setLocation(200,200);
        this.setTitle("Taquillero");
        this.setBackground(Color.CYAN);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JPanel p= new JPanel(new BorderLayout());
        JPanel panelCentro=new JPanel();
        JPanel panelNorte=new JPanel();
        JPanel panelSur=new JPanel();
        JPanel panelOeste=new JPanel();
        //Creamos un grid para indicar que nuestro marco es de 5 espacios Verticales como Horizontales
        GridBagConstraints gbc1 = new GridBagConstraints();
        GridBagLayout gbl1 = new GridBagLayout();
        //
        panelCentro.setLayout(gbl1);
        panelNorte.setLayout(gbl1);
        panelSur.setLayout(gbl1);
        panelOeste.setLayout(gbl1);
        panelCentro.setBackground(Color.GREEN);
        panelNorte.setBackground(Color.CYAN);
        panelSur.setBackground(Color.CYAN);
//        panelCentro.setOpaque(true);
        /*getContentPane();
        p.setLayout(new GridLayout(10,2,5,5));
        
        p.add(jl1);
        p.add(tq1);
        
        p.add(jl2);
        p.add(tq2);
        
        p.add(jl3);
        p.add(tq3);
        
        p.add(jl4);
        p.add(tq4);
        
        p.add(jl5);
        p.add(tq5);*/
        
        //Panel Sur
        //getContentPane();
        
        /*panelSur.setLayout(new GridLayout(2,0,5,10));
        gbc1.anchor=GridBagConstraints.CENTER;
        panelSur.add(jl6,gbc1);
        panelSur.add(bts,gbc1);*/
        //PANEL NORTE
        gbc1.gridx=0;
        gbc1.gridy=0;
        panelNorte.add(jl6,gbc1);
        gbc1.gridx=0;
        gbc1.gridy=1;
        panelNorte.add(bts,gbc1);
        gbc1.gridx=1;
        gbc1.gridy=0;
        
        //PANEL CENTRO
        gbc1.gridy=0;
        gbc1.gridx=2;
        panelCentro.add(jl9,gbc1);
        gbc1.gridy=0;
        gbc1.gridx=3;
        panelCentro.add(jl10,gbc1);
        gbc1.gridy=0;
        gbc1.gridx=1;
        gbc1.anchor = GridBagConstraints.CENTER;
        panelCentro.add(jl7,gbc1);
        gbc1.gridy=1;
        gbc1.gridx=1;
        panelCentro.add(jl1,gbc1);
        gbc1.gridy=2;
        gbc1.gridx=1;
        panelCentro.add(tq1,gbc1);
        gbc1.gridy=2;
        gbc1.gridx=2;
        panelCentro.add(tq6,gbc1);
        gbc1.gridy=2;
        gbc1.gridx=3;
        panelCentro.add(tq11,gbc1);
        gbc1.gridy=3;
        gbc1.gridx=1;
        panelCentro.add(jl2,gbc1);
        gbc1.gridy=4;
        gbc1.gridx=1;
        panelCentro.add(tq2,gbc1);
        gbc1.gridy=4;
        gbc1.gridx=2;
        panelCentro.add(tq7,gbc1);
        gbc1.gridy=4;
        gbc1.gridx=3;
        panelCentro.add(tq12,gbc1);
        gbc1.gridy=5;
        gbc1.gridx=1;
        panelCentro.add(jl3,gbc1);
        gbc1.gridy=6;
        gbc1.gridx=1;
        panelCentro.add(tq3,gbc1);
        gbc1.gridy=6;
        gbc1.gridx=2;
        panelCentro.add(tq8,gbc1);
        gbc1.gridy=6; //
        gbc1.gridx=3;
        panelCentro.add(tq13,gbc1);//
        gbc1.gridy=7;
        gbc1.gridx=1;
        panelCentro.add(jl4,gbc1);
        gbc1.gridy=8;
        gbc1.gridx=1;
        panelCentro.add(tq4,gbc1);
        gbc1.gridy=8;
        gbc1.gridx=2;
        panelCentro.add(tq9,gbc1);
        gbc1.gridy=8;
        gbc1.gridx=3;
        panelCentro.add(tq14,gbc1);
        gbc1.gridy=9;
        gbc1.gridx=1;
        panelCentro.add(jl5,gbc1);
        gbc1.gridy=10;
        gbc1.gridx=1;
        panelCentro.add(tq5,gbc1);
        gbc1.gridy=10;
        gbc1.gridx=2;
        panelCentro.add(tq10,gbc1);
        gbc1.gridy=10;
        gbc1.gridx=3;
        panelCentro.add(tq15,gbc1);
       /* gbc1.gridx=0;
        gbc1.gridy=1;
        panelNorte.add(jl6,gbc1);
        gbc1.gridx=0;
        gbc1.gridy=2;
        panelNorte.add(bts,gbc1);
        //PANEL CENTRO
        gbc1.gridy=0;
        gbc1.gridx=1;
        gbc1.anchor=GridBagConstraints.CENTER;
        panelCentro.add(jl7,gbc1);
        gbc1.gridy=1;
        gbc1.gridx=0;
        panelCentro.add(jl1,gbc1);
        gbc1.gridy=1;
        gbc1.gridx=1;
        panelCentro.add(tq1,gbc1);
        gbc1.gridy=1;
        gbc1.gridx=1;
        panelCentro.add(tq2,gbc1);
        gbc1.gridy=2;
        gbc1.gridx=0;
        panelCentro.add(jl3,gbc1);
        gbc1.gridy=2;
        gbc1.gridx=1;
        panelCentro.add(tq3,gbc1);
        gbc1.gridy=3;
        gbc1.gridx=0;
        panelCentro.add(jl4,gbc1);
        gbc1.gridy=3;
        gbc1.gridx=1;
        panelCentro.add(tq4,gbc1);
        gbc1.gridy=4;
        gbc1.gridx=0;
        panelCentro.add(jl5,gbc1);
        gbc1.gridy=4;
        gbc1.gridx=1;
        panelCentro.add(tq5,gbc1);*/
        //PanelOeste
       /*gbc1.gridy=2;
       gbc1.gridx=1;
       panelOeste.add(tq6,gbc1);
       gbc1.gridy=6;
       gbc1.gridx=1;
       panelOeste.add(tq7,gbc1);
       gbc1.gridy=8;
       gbc1.gridx=1;
       panelOeste.add(tq8,gbc1);
       gbc1.gridy=10;
       gbc1.gridx=1;
       panelOeste.add(tq9,gbc1);
       gbc1.gridy=12;
       gbc1.gridx=1;
       panelOeste.add(tq10,gbc1);*/
        
       //PANEL SUR
       gbc1.gridx=0;
       gbc1.gridy=0;
       panelSur.add(jl8);
        //gbc1.anchor=GridBagConstraints.CENTER;
        /*panelSur.add(jl6,gbc1);
        gbc1.gridy=1;
        gbc1.gridx=2;
        panelSur.add(bts,gbc1);*/
        
        p.add(panelCentro,BorderLayout.CENTER);
        p.add(panelNorte,BorderLayout.NORTH);
        p.add(panelSur,BorderLayout.SOUTH);
        p.add(panelOeste,BorderLayout.EAST);
        this.add(p);
        this.setVisible(true);
   // }
    }
    
}